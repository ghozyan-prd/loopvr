<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_pengunjung');
		$this->load->model('person_model');
	}
	function index(){
		if($this->session->userdata('akses')=='1'){
			$data['total_km'] = $this->person_model->hitungJumlahKm();
			$data['visitor'] = $this->m_pengunjung->statistik_pengujung();
			$this->load->view('admin/v_dashboard',$data);
		}else{
			redirect('administrator');
		}

	}

}
