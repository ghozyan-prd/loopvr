<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('depan/head') ?>
</head>
<body>
<?php $this->load->view('depan/header') ?>
<!--//END HEADER -->

    <!--//END HEADER -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-title">
                        <h2>Peserta Silahkan Login</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 contact-option">
                                <div class="contact-option_rsp">
                                    <h3>Form Login</h3>
                                    <form action="<?php echo site_url('Login/auth');?>" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Silahkan Isi Email anda" name="username" required>
                                        </div>
                                        <!-- // end .form-group -->
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Silahkan Isi Password" name="password" required>
                                        </div>
                                        <!-- // end .form-group -->
                                        <!-- // end .form-group -->
                                        <button type="submit" class="btn btn-default btn-submit">Login</button>
                                        <div><?php echo $this->session->flashdata('msg');?></div>
                                        <!-- // end #js-contact-result -->
                                    </form>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 contact-option">
                                <div class="contact-option_rsp">
                                    <h3>Form Register Baru</h3>
                                      <!-- // end .form-group -->
                                      <a href="http://localhost/loopvr/peserta/Signup_Controller" class="btn btn-default btn-submit" role="button">register</a>
                                      <!-- // end #js-contact-result -->
                                  </form>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p class="contact-center">OR</p>
                    </div>
                </div>
            </div>
        </section>
        <!--//END  ABOUT IMAGE -->
    <!--//End Style 2 -->
    <!--============================= FOOTER =============================-->
    <?php $this->load->view('depan/footer') ?>
        </body>

        </html>
