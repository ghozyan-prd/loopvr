<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('depan/head') ?>
</head>

<body>
    <?php $this->load->view('depan/header') ?>
      <section>
</section>
<!--//END HEADER -->
<!--//END ABOUT IMAGE -->
<!--============================= WELCOME TITLE =============================-->
<section class="content-section" id="portfolio">
      <div class="container">
        <div class="content-section-heading text-center">
          <h3 class="text-secondary mb-0">PANTI ASUHAN</h3>
          <h2 class="mb-5">yayasan yang sudah bekerja sama dengan kami</h2>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Panti Asuhan Ar Rochim</h2>
                  <p class="mb-0">25 anak telah diasuh, nomer rekening 1410013694989</p>
                </span>
              </span>
              <img class="img-fluid" src="theme/images/portfolio-1.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Yayasan Mitra Arafah</h2>
                  <p class="mb-0">25 anak telah diasuh, nomer rekening 1410013</p>
                </span>
              </span>
              <img class="img-fluid" src="theme/images/portfolio-2.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Strawberries</h2>
                  <p class="mb-0">Strawberries are such a tasty snack, especially with a little sugar on top!</p>
                </span>
              </span>
              <img class="img-fluid" src="theme/images/portfolio-3.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Coming Soon</h2>
                  <p class="mb-0">A yellow workspace with some scissors, pencils, and other objects.</p>
                </span>
              </span>
              <img class="img-fluid" src="theme/images/portfolio-4.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
    </section>
    <!--//END WELCOME TITLE -->
    <!--============================= TESTIMONIAL =============================-->
   <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h3 class="text-secondary mb-0">Donasi</h3>
          <h2 class="mb-5">Kami Menerima Berbagai Transaksi</h2>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/Bitcoin.png" alt="">
            </span>
            <h4>
              <strong>Bitcoin</strong>
            </h4>
            <h10 class="text-faded mb-0">1A6jvTEKDHjY34xN2L44rqQdSjiWoC1PN8</h10>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/ETHEREUM-YOUTUBE-PROFILE-PIC.png" alt="">
            </span>
            <h4>
              <strong>Ethereum</strong>
            </h4>
            <h4 class="text-faded mb-0">0xb70EaeEF822ee116339Dd2</h4>
            <h4 class="text-faded mb-0">F3Ba323bac339be590</h4>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/logobtn-min.png" alt="">
            </span>
            </span>
            <h4>
              <strong>Bank BTN</strong>
            </h4>
            <p class="text-faded mb-0">0006401610219780</p>
          </div>
          <div class="col-lg-3 col-md-6">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/money-png-40450.png" alt="">
            </span>
            <h4>
              <strong>Tunai</strong>
            </h4>
            <p class="text-faded mb-0">Bayar ditempat</p>
          </div>
        </div>
      </div>
    </section>
    <!--//END TESTIMONIAL -->
    <!--============================= DETAILED CHART =============================-->
    <div class="detailed_chart">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_1.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_guru;?></span> Pengurus
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom chart_top">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_2.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_siswa;?></span> Siswa
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 chart_top">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_3.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_files;?></span> Download
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_4.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_agenda;?></span> Agenda</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//END DETAILED CHART -->

        <!--============================= FOOTER =============================-->
      <?php $this->load->view('depan/footer') ?>
            </body>

            </html>
